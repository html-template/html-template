;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-
;;; $Header: /usr/local/cvsrep/html-template/packages.lisp,v 1.13 2004/04/24 00:17:43 edi Exp $

;;; Copyright (c) 2003, Dr. Edmund Weitz. All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package #:cl-user)

#-:cormanlisp
(defpackage #:html-template
  (:nicknames #:template)
  (:use #:cl)
  (:export #:fill-and-print-template
           #:create-template-printer
           #:delete-from-template-cache
           #:clear-template-cache
           #:*template-start-marker*
           #:*template-end-marker*
           #:*default-template-pathname*
           #:*default-template-output*
           #:*convert-nil-to-empty-string*
           #:*sequences-are-lists*
           #:*upcase-attribute-strings*
           #:*no-cache-check*
           #:*template-symbol-package*
           #:*value-access-function*
           #:*ignore-empty-lines*
           #:*warn-on-creation*
           #:template-error
           #:template-invocation-error
           #:template-missing-value-error
           #:template-not-a-string-error
           #:template-not-a-string-error-value
           #:template-syntax-error
           #:template-syntax-error-line
           #:template-syntax-error-col
           #:template-syntax-error-stream))

#+:cormanlisp
(defpackage "HTML-TEMPLATE"
  (:nicknames "TEMPLATE")
  (:use "CL")
  (:export "FILL-AND-PRINT-TEMPLATE"
           "CREATE-TEMPLATE-PRINTER"
           "DELETE-FROM-TEMPLATE-CACHE"
           "CLEAR-TEMPLATE-CACHE"
           "*TEMPLATE-START-MARKER*"
           "*TEMPLATE-END-MARKER*"
           "*DEFAULT-TEMPLATE-PATHNAME*"
           "*DEFAULT-TEMPLATE-OUTPUT*"
           "*CONVERT-NIL-TO-EMPTY-STRING*"
           "*SEQUENCES-ARE-LISTS*"
           "*UPCASE-ATTRIBUTE-STRINGS*"
           "*NO-CACHE-CHECK*"
           "*TEMPLATE-SYMBOL-PACKAGE*"
           "*VALUE-ACCESS-FUNCTION*"
           "*IGNORE-EMPTY-LINES*"
           "*WARN-ON-CREATION*"
           "TEMPLATE-ERROR"
           "TEMPLATE-INVOCATION-ERROR"
           "TEMPLATE-MISSING-VALUE-ERROR"
           "TEMPLATE-NOT-A-STRING-ERROR"
           "TEMPLATE-NOT-A-STRING-ERROR-VALUE"
           "TEMPLATE-SYNTAX-ERROR"
           "TEMPLATE-SYNTAX-ERROR-LINE"
           "TEMPLATE-SYNTAX-ERROR-COL"
           "TEMPLATE-SYNTAX-ERROR-STREAM"))

(pushnew :html-template *features*)